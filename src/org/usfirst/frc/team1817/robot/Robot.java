package org.usfirst.frc.team1817.robot;

import edu.wpi.first.wpilibj.*;
import edu.wpi.first.wpilibj.smartdashboard.*;

public class Robot extends SampleRobot{
    private XboxController driveXbox, manipulatorXbox;
    private VictorSP left1, left2, right1, right2, climber, gears, shooterFront, shooterBack;
    private RobotDrive drive;
    private Compressor compressor;
    private DoubleSolenoid shifter, grabber;
    private DigitalInput gearSwitch, pegSwitch;
    private Encoder leftE, rightE, shootE;
    private AnalogInput gearPot;
    
    private GearPOT gearPOT;
    private Rumble gearRumble;
    
    private Toggle tankDrive, shifterForward, grabberForward;
    
    private SendableChooser autonChooser;
    
    private double shootSpeed;
    private boolean shootPeaked;
    private boolean gearSwitched;
    private boolean dontPeg;
    
    public Robot(){ // Initializations
        // Joysticks/Controllers
        driveXbox       = new XboxController(0);
        manipulatorXbox = new XboxController(1);
        
        // Motors
        left1        = new VictorSP(5); //0
        left2        = new VictorSP(6); //1
        right1       = new VictorSP(0); //5
        right2       = new VictorSP(1); //6
        climber      = new VictorSP(2);
        gears        = new VictorSP(3);
        shooterBack  = new VictorSP(8);
        shooterFront = new VictorSP(9); // Splitter for two motors
        
        // Drive Groups
        drive = new RobotDrive(left1, left2, right1, right2);
        
        // Compressor
        compressor = new Compressor();
        
        // Solenoids
        shifter = new DoubleSolenoid(0, 7);
        grabber = new DoubleSolenoid(2, 5);
        
        // Solenoid Defaults
        shifter.set(DoubleSolenoid.Value.kForward);
        grabber.set(DoubleSolenoid.Value.kForward);
        
        // Digital Inputs
        gearSwitch = new DigitalInput(0);
        pegSwitch  = new DigitalInput(5);
        
        // Encoders
        leftE  = new Encoder(2, 1);
        rightE = new Encoder(3, 4);
        shootE = new Encoder(7, 6);
        
        double encoderPulse = Math.PI * 4 / 1450.0 / 12.0;
        leftE.setDistancePerPulse(encoderPulse);
        rightE.setDistancePerPulse(encoderPulse);
        shootE.setDistancePerPulse(60.0 / Math.PI / 2);
        
        new EncoderWatcher(leftE, "Left Encoder");
        new EncoderWatcher(rightE, "Right Encoder");
        new EncoderWatcher(shootE, "Shooter Encoder");
        
        // Analog Input
        gearPot = new AnalogInput(0);
        
        // Separate Threads
        gearPOT = new GearPOT(gears, gearPot, 3.85, .75, 2.4);
        gearPOT.up();
        gearRumble = new Rumble(driveXbox);
        new PowerMonitor();
        new RobotCam();
        
        // Toggles
        tankDrive      = new Toggle();
        shifterForward = new Toggle();
        grabberForward = new Toggle();
        
        autonChooser = new SendableChooser();
        autonChooser.addDefault("Gear Center", 0);
        autonChooser.addObject("Drive Forward Only", 1);
        SmartDashboard.putData("Autonomous", autonChooser);
        
        shootSpeed   = 0.5;
        shootPeaked  = false;
        gearSwitched = false;
        dontPeg      = false;
    }
    
    @Override
    public void autonomous(){
        setSafety(false);
        
        gearPOT.up();
        
        shifter.set(DoubleSolenoid.Value.kReverse);
        grabber.set(DoubleSolenoid.Value.kForward);
        
        if((int)autonChooser.getSelected() == 0){
            autoDrive(-9.25);
            autoPeg();
        } else if((int)autonChooser.getSelected() == 1){
            autoDrive(-9.25);
        }
        
        setSafety(true);
    }
    
    public void setSafety(boolean enabled){ // Add EVERY Motor and Drive Group to this function
        // Motors
        left1.setSafetyEnabled(enabled);
        left2.setSafetyEnabled(enabled);
        right1.setSafetyEnabled(enabled);
        right2.setSafetyEnabled(enabled);
        climber.setSafetyEnabled(enabled);
        gears.setSafetyEnabled(enabled);
        
        // Drive Groups
        drive.setSafetyEnabled(enabled);
    }
    
    public void deadband(PWMSpeedController c, double in){
        if(Math.abs(in) < 0.1){
            c.stopMotor();
        } else {
            c.set(in);
        }
    }
    
    public double calcSpeed(double max, double current, double target){
        return max * Math.max(-1.0, Math.min(target - current, 1.0));
    }
    
    public void autoDrive(double dist){
        leftE.reset();
        rightE.reset();
        
        double time = Timer.getFPGATimestamp();
        double max  = 0.7;
        while((Timer.getFPGATimestamp() - time < 3.0) && rightE.getDistance() > dist && !pegSwitch.get()){
            double leftD = leftE.getDistance();
            double rightD = rightE.getDistance();
            
            double leftSpeed  = -calcSpeed(max, leftD, dist);
            double rightSpeed = -calcSpeed(max, rightD, dist);
            
            if(Math.abs(leftD) > Math.abs(rightD)){
                double diff = (Math.abs(leftD) - Math.abs(rightD));
                if(dist >= 0){
                    leftSpeed += diff;
                } else {
                    leftSpeed -= diff;
                }
            } else {
                double diff = (Math.abs(rightD) - Math.abs(leftD));
                if(dist >= 0){
                    rightSpeed += diff;
                } else {
                    rightSpeed -= diff;
                }
            }
            
            drive.tankDrive(leftSpeed, rightSpeed);
        }
        drive.stopMotor();
    }
    
    public void autoPeg(){
        drive.stopMotor();
        
        gearPOT.down();
        Timer.delay(0.1);
        grabber.set(DoubleSolenoid.Value.kReverse);
        grabberForward.set(false);
        Timer.delay(0.5);
        
        double time = Timer.getFPGATimestamp();
        double dist = 2.0;
        double max  = 0.8;
        rightE.reset();
        leftE.reset();
        while((Timer.getFPGATimestamp() - time < 1.0) && rightE.getDistance() < dist && leftE.getDistance() < dist){
            double leftSpeed  = -calcSpeed(max, leftE.getDistance(), dist);
            double rightSpeed = -calcSpeed(max, rightE.getDistance(), dist);
            drive.tankDrive(leftSpeed, rightSpeed);
        }
        
        gearPOT.up();
    }
    
    public void drive(){
        double leftY = driveXbox.getY(GenericHID.Hand.kLeft);
        double rightX = driveXbox.getX(GenericHID.Hand.kRight);
        double rightY = driveXbox.getY(GenericHID.Hand.kRight);
        
        tankDrive.update(driveXbox.getStartButton());
        
        if(tankDrive.toggled() && (Math.abs(leftY) > 0.1 || Math.abs(rightY) > 0.1)){
            drive.tankDrive(leftY, rightY);
        } else if(Math.abs(rightX) > 0.1 || Math.abs(leftY) > 0.1){
            drive.arcadeDrive(leftY, rightX);
        } else {
            drive.stopMotor();
        }
        
        shifter.set(driveXbox.getBumper(GenericHID.Hand.kRight) ? DoubleSolenoid.Value.kForward : DoubleSolenoid.Value.kReverse);
    }
    
    public void climber(){
        double manLT = manipulatorXbox.getTriggerAxis(GenericHID.Hand.kLeft);
        double manRT = manipulatorXbox.getTriggerAxis(GenericHID.Hand.kRight);
        deadband(climber, -manLT - manRT);
    }
    
    public void shooter(){
        if(manipulatorXbox.getAButton()){
            dontPeg = true;
            double ideal = SmartDashboard.getNumber("Shooter Velocity", 5500);
            
            shootSpeed += Math.min((shootE.getRate() - ideal) / 10000, 1.0);
            
            if(shootE.getRate() > ideal){
                shootPeaked = true;
            }
            
            if(shootPeaked){
                shooterBack.set(-0.8);
            } else {
                shooterBack.stopMotor();
            }
            
            shooterFront.set(shootSpeed);
        } else if(manipulatorXbox.getYButton()){
            shooterFront.stopMotor();
            shooterBack.set(0.5);
        } else {
            shootSpeed = 0.5;
            shootPeaked = false;
            if(!pegSwitch.get()) dontPeg = false;
            shooterFront.stopMotor();
            shooterBack.stopMotor();
        }
    }
    
    public void gearPos(){
        if(driveXbox.getAButton()){
            gearPOT.down();
        } else if(driveXbox.getXButton()){
            gearPOT.score();
        } else if(driveXbox.getYButton()){
            gearPOT.up();
        }
    }
    
    public void gearGrab(){
        grabberForward.update(driveXbox.getBButton());
        
        if(gearSwitch.get()){
            if(!gearSwitched){
                gearSwitched = true;
                grabberForward.set(true);
                gearRumble.rumbleForTime(1.0);
            }
        } else {
            gearSwitched = false;
        }
        
        grabber.set(grabberForward.toggled() ? DoubleSolenoid.Value.kForward : DoubleSolenoid.Value.kReverse);
    }
    
    @Override
    public void operatorControl(){
        setSafety(true);
        compressor.start();
        
        SmartDashboard.putNumber("Shooter Velocity", 5500);
        
        shootSpeed   = 0.5;
        shootPeaked  = false;
        gearSwitched = false;
        dontPeg      = false;
        
        while(isEnabled() && isOperatorControl()){
            if(pegSwitch.get() && !dontPeg){
                dontPeg = true;
                autoPeg();
            } else {
                drive();
                climber();
                
                shooter();
                
                gearPos();
                gearGrab();
            }
            
            Timer.delay(0.005);
        }
    }
    
    @Override
    public void test(){
        
    }
}
